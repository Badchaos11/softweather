package middleware

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

const (
	accessHeaderKey   = "User-Access"
	accessHeaderValue = "superuser"
)

func UserAccessMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessHeader := r.Header.Get(accessHeaderKey)
		if accessHeader == "" {
			logrus.Error("User-Access must be set")
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Заголовок User-Access должен быть обязательно установлен."))
			return
		}
		if accessHeader != accessHeaderValue {
			logrus.Errorf("User-Access has value %s", accessHeader)
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Недопустимое значение User-Access"))
			return
		}

		next.ServeHTTP(w, r)
	})
}
