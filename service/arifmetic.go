package service

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"github.com/moxar/arithmetic"
)

type response struct {
	Result       float64 `json:"result"`
	ErrorMessage string  `json:"error"`
}

type request struct {
	Expression string `json:"expression"`
}

func (s *Service) SolveExpression(w http.ResponseWriter, r *http.Request) {

	var expr request
	body, err := io.ReadAll(r.Body)
	if err != nil {
		resp := response{
			ErrorMessage: "Ошбика при чтении тела запроса",
		}
		data, err := json.Marshal(resp)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(data)
		return
	}

	if err := json.Unmarshal(body, &expr); err != nil {
		resp := response{
			ErrorMessage: "Ошбика при десериализации запроса",
		}
		data, err := json.Marshal(resp)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(data)
		return
	}

	if expr.Expression == "" {
		resp := response{
			ErrorMessage: "Передано пустое выражение",
		}
		data, err := json.Marshal(resp)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(data)
		return
	}

	if checkMultOrDiv(expr.Expression) {
		resp := response{
			ErrorMessage: "Доступны только сложение и вычитание",
		}
		data, err := json.Marshal(resp)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(data)
		return
	}

	res, err := arithmetic.Parse(expr.Expression)
	if err != nil {
		resp := response{
			ErrorMessage: "Ошибка при разборе выражения",
		}
		data, err := json.Marshal(resp)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(data)
		return
	}

	resp := response{
		Result: res.(float64),
	}
	data, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}

func checkMultOrDiv(expr string) bool {
	return strings.Contains(expr, "*") || strings.Contains(expr, "/")
}
