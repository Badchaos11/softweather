package service

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/softweather/middleware"
)

type Service struct {
	port int64
}

func New() *Service {
	return &Service{
		port: 3000,
	}
}

func (s *Service) Run() {

	router := mux.NewRouter()

	router.Use(middleware.UserAccessMiddleware)
	router.HandleFunc("/api/v1/solve-sxpression", s.SolveExpression).Methods("POST")

	server := &http.Server{
		Addr:         fmt.Sprintf(":%d", s.port), // Порт сервера
		Handler:      router,                     // Хэндлеры
		ReadTimeout:  1 * time.Minute,            // Таймаут запроса клиента
		WriteTimeout: 1 * time.Minute,            // Таймаут ответа клиенту
		IdleTimeout:  120 * time.Second,          // Таймаут соединения в простое
	}

	go func() {
		logrus.Infof("starting server on port %v", s.port)

		err := server.ListenAndServe()
		if err != nil {
			logrus.Errorf("error starting server %v", err)
			os.Exit(1)
		}
	}()

	// Отключение
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	sig := <-c
	logrus.Infof("Got signal: %v", sig)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	server.Shutdown(ctx)
}

func init() {

	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		ForceColors:     true,
		DisableColors:   false,
		FullTimestamp:   true,
	})
	logrus.SetLevel(logrus.DebugLevel)
}
