module gitlab.com/Badchaos11/softweather

go 1.20

require github.com/gorilla/mux v1.8.0

require (
	github.com/moxar/arithmetic v0.1.0 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
